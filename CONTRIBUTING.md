# Contributing
Hjälp gärna till att dela information om vilken organisation som använder öppen programvara på ett ansvarsfullt sätt.

Offentliga organisationer såsom stat, landsting och kommuner har tusentals installationer av programvaror. Det finns en stor mängd outnyttjad kunskap om användning som kan delas i en katalog för att stödja val av öppna alternativ. Huvudsyftet med en sådan katalog är att underlätta kontakten och erfarenhetsutbytet mellan olika offentliga organisationer vid beslut om byte, avinstallation, införande eller digitalisering av programvaror. Att kunna söka kunskap hos andra tjänstemän, som ett komplement till att bara prata med marknaden, upplevs som värdefullt och kan bidra till att göra mer informerade beslut om programvaruval.

> Det är viktigt att notera att katalogen inte garanterar att listade programvaror är funktionella, kostnadseffektiva, säkra eller pålitliga. Katalogen ger endast en kontaktväg och en uppfattning om popularitet, vilket ej är en garanti för att programvaran är rätt val.

## Kriterier och regler för att bidra med innehåll

*Programvaran ska vara öppen källkod och användas av en offentlig aktör.*

1.	Med öppen källkod menas att källkoden är tillgänglig för alla att ta del utav och har en godkänd licens av [OSI](https://opensource.org/licenses/).
2.	Den part som delar innehåll måste godkänna att informationen publiceras på offentligkod.se. Vad som delas är NAMN på programvaran och ORGANISATIONEN som använder programvaran.
3.	Informationsklassningen för NAMN och ORGANISATION är offentlig information i de flesta fallen. Vid minsta tvivel så avstå från att dela. Förutom att det både ska vara lagligt och lämpligt att dela så ska delningen upplevas allmännyttig.

### Motsatsernas dynamik
Notera att inga personuppgifter delas. Då syftet är att kunna kontakta en organisation får det antas vara motsägelsefullt att det ej finns en kontaktväg. Det är ett olöst problem! Men tills vidare är hypotesen att katalogens användare hjälper varandra att hitta rätt kontaktvägar för respektive organisation. Det finns även flera internetforum som dialoger om data och programvara kan föras.

## Manual för delning

Konkret finns det tre sätt att dela och nedan mall ska användas.

1. Via att skicka ett email
2. Skapa en issue i detta repo enligt en mall
3. Ändra i databasen själv via en MergeRequest. (Ja, databasen är en textfil)

**Mall**

| Fält | Att tänka på |
| ------ | ------ |
|Id|Varje rad måste ha ett unikt nummer. Om fältet exkluderas så lägger scriptet på ett nummer i efterprocesseringen.|
|Url| Obligatorisk. En textsträng som länkar till mer information om programvaran. Antingen en länk till källkoden eller till en officiell sajt som beskriver programvaran.|
|Description|Obligatorisk. En beskrivning av programvaran på ca 3 meningar. Raden ska brytas upp med + efter ca 60-75 tecken. Scriptet som läser databasen tar bort plustecknet. |
|Keyword|Obligatorisk. Ett nyckolord från en fördefinerad lista. Listan återfinns högst uppe i filen programvaror.rec. Sajten har en fritextsökning, så fältet är inte helt avgörande för sökningen. Lägg hellre tid på de tre beskrivande meningarna än det perfekta ordet som klassificerar programvaran. |
|User|Obligatorisk. En rad per användare av programvaran. Detta värde måste vara definierat i filen organisationer.rec. Viktigt att nyckeln är förståbar. Ex nyckeln till Arbetsförmedlingen är arbetsformedlingen.|


### Alternativ 1 - Skicka mail

Skicka ett mail till jonas [dot] sodergren [at] arbetsformedlingen [dot] se.

Detta förslag är personberoende och bör undvikas. Men alternativet erbjuds för att det ska vara enkelt för alla att bidra. Obligatoriskt att dela URL, Name och User. Description kan utelämnas men leder till att någon annan kommer göra den beskrivningen. Det kan innebära att tillägget fördröjs någon dag.

### Alternativ 2 - Skapa en issue

[Länk till issuetrackern enligt ovan mall för hur bidraget ska fyllas i.](https://gitlab.com/open-data-knowledge-sharing/katalogen/-/issues/new?issuable_template=ny-programvara&issue[title]=F%C3%B6rslag%20p%C3%A5%20till%C3%A4gg) Bidraget granskas och läggs till manuellt. Det finns en lösning för hur det kan ske automatiskt som är svår att sätta i bruk. Utmaningen är att det behöver göras en rimlighetskontroll att bidraget är lämpligt att publicera. Detta alternativ är att föredra då det automatiskt går att följa ärenden för tillägg.

### Alternativ 3 - Ändra i databasen via en MergeRquest

Listan med programvaror är en fil som är läsbar och editerbar (av människor). Databassystemet är enkelt med kraftfullt. Se [recutils](https://www.gnu.org/software/recutils/). Lägg gärna till rader i databasen som består av två filer enligt en MergeRequest. En MergeRequest avseende tillägg av en programvara ska enbart innehålla förändringar i nedanstående filer.

1. [Organisationer](db/organisationer.rec)
2. [Programvaror](db/programvaror.rec)

**Exempel på en ny rad i databasen**

```
Id: 1
Name: Alfresco
Url: https://www.alfresco.com/ecm-software/alfresco-community-editions
Description:
+ Alfresco är en open source-plattform som används för att
+ hantera innehåll för en organisation. Det erbjuder en rad
+ funktioner som dokumenthantering, samarbete, delning och
+ spårning av innehåll.
Keyword: cms
User: vgr_r
User: havochvatten
User: svenska_kraftn
User: alingsas_k
User: lund_uh
User: smhi
```

> Notera att det är tvingande att lägga till ett Id för tillägget.

TODO:
- [ ] Id bör sättas automatiskt av databashanteraren
- [ ] Ändra från löpnummer till UUID för Id.
